FROM node:16-alpine3.11 as build
WORKDIR /app/frontend
COPY package.json package-lock.json ./
RUN npm install 
RUN npm install react-scripts@4.0.3 -g  
COPY . ./

EXPOSE 3000

RUN npm run build

FROM nginx:stable-alpine
COPY --from=build /app/frontend/build /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf


